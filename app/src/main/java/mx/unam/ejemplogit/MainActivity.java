package mx.unam.ejemplogit;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


/**
 * @author urielros
 * @author Emmanuel Peto
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
